# coding=utf-8

# Created by: Kornél Tóth
# 2020. 10. 03.

import string
letters = string.ascii_uppercase

#function for encryption
def encrypt_vignere(plain, key):
    key = int(len(plain) / len(key)) * key + key[:(len(plain) % len(key))]
    cipher = []
    key = key.upper()
    key_index = 0
    for i in plain:
        index = letters.find(i.upper())
        if index != -1: #-1 == not in letters
            index = (index + letters.find(key[key_index])) % 26
            if i.isupper():
                cipher.append(letters[index])
            elif i.islower():
                cipher.append(letters[index].lower())
            key_index += 1
        else:
            cipher.append(i)
    return ''.join(cipher)

#function for decryption
def decrypt_vignere(cipher, key):
    key = int(len(cipher) / len(key)) * key + key[:(len(cipher) % len(key))]
    plain = []
    key = key.upper()
    key_index = 0
    for i in cipher:
        index = letters.find(i.upper())
        if index != -1: #-1 == not in letters
            index = (index - letters.find(key[key_index])) % 26
            if i.isupper():
                plain.append(letters[index])
            elif i.islower():
                plain.append(letters[index].lower())
            key_index += 1
        else:
            plain.append(i)
    return ''.join(plain)

#Implement attack
import itertools, re
most_freq_letters = 4 #amount of letters that the algorithm will attempt / subkey
max_key_length = 7 #can be changed
nonletter_pattern = re.compile('[^A-Z]')

def find_repeated_sequences(text):
    #remove non-letter characters
    text = nonletter_pattern.sub('', text.upper())
    sequence_spacings = {}
    for sequence_len in range(3, 6):
        for sequence_start in range(len(text) - sequence_len):
            sequence = text[sequence_start:sequence_start + sequence_len]
            #find this sequence
            for i in range(sequence_start + sequence_len, len(text) - sequence_len):
                if text[i:i + sequence_len] == sequence:
                    #found a matching seqence
                    if sequence not in sequence_spacings:
                        sequence_spacings[sequence] = []
                        sequence_spacings[sequence].append(i - sequence_start)
                        return sequence_spacings

def find_nonone_factors(number):
    #This will get us all non-one factors of number, that are lower than or equal to max_key_length
    prime_factors = factor(number)
    small_factors = list(set([f[0] for f in prime_factors for _ in range(f[1]) if f[0] < max_key_length]))
    factors = [int(number / f) for f in small_factors if int(number / f) != 1] + small_factors
    return factors

def find_most_common_factors(factor_sequence):
    factor_count = {}
    for sequence in factor_sequence:
        factor_list = factor_sequence[sequence]
        for factor in factor_list:
            if factor not in factor_count:
                factor_count[factor] = 0
            factor_count[factor] += 1
    #make a list of tuples from factors and there counts
    factor_by_count = []
    for factor in factor_count:
        if factor <= max_key_length:
            factor_by_count.append((factor, factor_count[factor]))
    #sort list
    factor_by_count.sort(key = lambda x : x[1], reverse = True)
    return factor_by_count

#implementing the Kasiski examination (https://en.wikipedia.org/wiki/Kasiski_examination)
def Kasiski(cipher):
    rep_sequential_spasings = find_repeated_sequences(cipher)
    sequence_factors = {}
    #print(rep_sequential_spasings)
    for sequence in rep_sequential_spasings:
        sequence_factors[sequence] = []
        for space in rep_sequential_spasings[sequence]:
            sequence_factors[sequence].extend(find_nonone_factors(space))

    factors_by_count = find_most_common_factors(sequence_factors)
    key_length = []
    for i in factors_by_count:
        key_length.append(i[0])
    return key_length

def nth_subkey(n, length, text):
    text = nonletter_pattern.sub('', text) #reg. exp. for removing non-letter carachters
    ind = n - 1
    letters = []
    while ind < len(text):
        letters.append(text[ind])
        ind += length
    return ''.join(letters)

#frequency finder for dictionary attack. source: https://inventwithpython.com/hacking/chapter20.html
english_letter_freq = {'E': 12.70, 'T': 9.06, 'A': 8.17, 'O': 7.51, 'I': 6.97, 'N': 6.75, 'S': 6.33, 'H': 6.09, 'R': 5.99, 'D': 4.25, 'L': 4.03, 'C': 2.78, 'U': 2.76, 'M': 2.41, 'W': 2.36, 'F': 2.23, 'G': 2.02, 'Y': 1.97, 'P': 1.93, 'B': 1.29, 'V': 0.98, 'K': 0.77, 'J': 0.15, 'X': 0.15, 'Q': 0.10, 'Z': 0.07}
ETAOIN = 'ETAOINSHRDLCUMWFGYPBVKJXQZ'

def letter_count(text):
    letter_count = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0, 'G': 0, 'H': 0, 'I': 0, 'J': 0, 'K': 0, 'L': 0, 'M': 0, 'N': 0, 'O': 0, 'P': 0, 'Q': 0, 'R': 0, 'S': 0, 'T': 0, 'U': 0, 'V': 0, 'W': 0, 'X': 0, 'Y': 0, 'Z': 0}
    for l in text.upper():
        if l in letters:
            letter_count[l] += 1

    return letter_count

def frequency_order(text):
    letter_to_frequency = letter_count(text)
    frequency_to_letter = {}
    for l in letters:
        if letter_to_frequency[l] not in frequency_to_letter:
            frequency_to_letter[letter_to_frequency[l]] = [l]
        else:
            frequency_to_letter[letter_to_frequency[l]].append(l)
    for f in frequency_to_letter:
        frequency_to_letter[f].sort(key=ETAOIN.find, reverse=True)
        frequency_to_letter[f] = ''.join(frequency_to_letter[f])
    frequency_pairs = list(frequency_to_letter.items())
    frequency_pairs.sort(key= lambda x : x[0], reverse=True)
    frequency_order = []
    for pair in frequency_pairs:
        frequency_order.append(pair[1])
    return ''.join(frequency_order)

def english_frequency_score(text): #returns the letter frequency compared to English language
    order = frequency_order(text)
    score = 0
    for cl in ETAOIN[:6]: #cl -> common letter
        if cl in order[:6]:
            score += 1
    for ul in ETAOIN[-6:]: #ul -> uncommon letter
        if ul in order[-6:]:
            score += 1
    return score

#english language checker for dictionary attack. source: https://inventwithpython.com/detectEnglish.py
def load_dictionary():
    file = open('dictionary.txt')
    english_words = {}
    for word in file.read().split('\n'):
        english_words[word] = None
    file.close()
    return english_words

english_words = load_dictionary()

def english_count(text):
    text = text.upper()
    text = remove_non_letters(text)
    possible_words = text.split()
    if possible_words == []:
        return 0.0
    matches = 0
    for word in possible_words:
        if word in english_words:
            matches += 1
    return float(matches) / len(possible_words)

def remove_non_letters(text):
    letters_only = []
    for symbol in text:
        if symbol in (letters + letters.lower() + ' \t\n'):
            letters_only.append(symbol)
    return ''.join(letters_only)

def is_english(text, word_percentage=20, letter_percentage=85):
    word_match = english_count(text) * 100 >= word_percentage
    num_letters = len(remove_non_letters(text))
    text_letter_percentage = float(num_letters) / len(text) * 100
    letter_match = text_letter_percentage >= letter_percentage
    return word_match and letter_match

def attack_with_key_length(cipher, key_length):
    cipher = cipher.upper()
    all_frequency_scores = []
    for n in range(1, key_length + 1):
        nth_letters = nth_subkey(n, key_length, cipher)
        frequency_scores = []
        for key in letters: #letters was declared at the beginning of the attack
            decrypted = decrypt_vignere(nth_letters ,key)
            key_and_score = (key, english_frequency_score(decrypted))
            frequency_scores.append(key_and_score)
        frequency_scores.sort(key= lambda x : x[1], reverse=True)
        all_frequency_scores.append(frequency_scores[:4]) #tries 4 letters per subkey
    for index in itertools.product(range(4), repeat = key_length):
        possible_key = ''
        for i in range(key_length):
            possible_key += all_frequency_scores[i][index[i]][0]
        decrypted = decrypt_vignere(cipher, possible_key)
        if is_english(decrypted):
            original_case = []
            for i in range(len(cipher)):
                if cipher[i].isupper():
                    original_case.append(decrypted[i].upper())
                else:
                    original_case.append(decrypted[i].lower())
            decrypted = ''.join(original_case)
            return decrypted
    return None

def vignere_attack(cipher): #The attack itself
    all_possible_key_lengths = Kasiski(cipher)
    for length in all_possible_key_lengths:
        decrypted = attack_with_key_length(cipher, length)
        if decrypted != None:
            break
    if decrypted == None:
        for l in range(1, max_key_length+1):
            if l not in all_possible_key_lengths:
                decrypted = attack_with_key_length(cipher, l)
                if decrypted != None:
                    break
    return decrypted

def main():
	valid_text = "Advent of modern cryptography Cryptanalysis of the new mechanical devices proved to be both difficult and laborious. In the United Kingdom, cryptanalytic efforts at Bletchley Park during WWII spurred the development of more efficient means for carrying out repetitious tasks. This culminated in the development of the Colossus, the worlds first fully electronic, digital, programmable computer, which assisted in the decryption of ciphers generated by the German Armys Lorenz SZ40/42 machine. Extensive open academic research into cryptography is relatively recent; it began only in the mid-1970s. In recent times, IBM personnel designed the algorithm that became the Federal (i.e., US) Data Encryption Standard; Whitfield Diffie and Martin Hellman published their key agreement algorithm;[31] and the RSA algorithm was published in Martin Gardner's Scientific American column. Following their work in 1976, it became popular to consider cryptography systems based on mathematical problems that are easy to state but have been found difficult to solve. Since then, cryptography has become a widely used tool in communications, computer networks, and computer security generally. Some modern cryptographic techniques can only keep their keys secret if certain mathematical problems are intractable, such as the integer factorization or the discrete logarithm problems, so there are deep connections with abstract mathematics. There are very few cryptosystems that are proven to be unconditionally secure. The one-time pad is one, and was proven to be so by Claude Shannon. There are a few important algorithms that have been proven secure under certain assumptions. For example, the infeasibility of factoring extremely large integers is the basis for believing that RSA is secure, and some other systems, but even so proof of unbreakability is unavailable since the underlying mathematical problem remains open. In practice, these are widely used, and are believed unbreakable in practice by most competent observers. There are systems similar to RSA, such as one by Michael O. Rabin that are provably secure provided factoring n = pq is impossible; it is quite unusable in practice. The discrete logarithm problem is the basis for believing some other cryptosystems are secure, and again, there are related, less practical systems that are provably secure relative to the solvability or insolvability discrete log problem. As well as being aware of cryptographic history, cryptographic algorithm and system designers must also sensibly consider probable future developments while working on their designs. For instance, continuous improvements in computer processing power have increased the scope of brute-force attacks, so when specifying key lengths, the required key lengths are similarly advancing.[34] The potential effects of quantum computing are already being considered by some cryptographic system designers developing post-quantum cryptography; the announced imminence of small implementations of these machines may be making the need for preemptive caution rather more than merely speculative."
	#source: https://en.wikipedia.org/wiki/Cryptography

	invalid_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce orci mauris, gravida in gravida a, consectetur eu sem. Quisque lorem magna, malesuada vel iaculis a, feugiat nec mauris. Donec tincidunt ante ut felis feugiat vulputate. Etiam at diam euismod, fermentum nisl vitae, placerat massa. Suspendisse ullamcorper odio et dui faucibus condimentum. Praesent mauris libero, condimentum a fermentum nec, ullamcorper quis nunc. Aliquam elementum ante auctor nibh laoreet iaculis. Phasellus vel rutrum turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed cursus magna quis purus ultrices consectetur eu non mauris. Donec suscipit aliquam tincidunt. Integer auctor est sed sem bibendum, ac viverra odio luctus. Nullam risus tortor, sagittis eu volutpat ut, aliquet id nulla. Cras imperdiet sit amet dolor in porta. Quisque vel leo quam. Pellentesque auctor scelerisque pellentesque. Ut fringilla ut diam non tempus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque maximus tortor arcu, sodales consectetur dolor consequat sed. Nunc sit amet dignissim urna, sit amet consequat enim. Aenean arcu ipsum, vehicula id diam sit amet, porttitor convallis orci. In ut vehicula erat, ultricies pretium dolor. Cras egestas neque et felis viverra placerat. In hac habitasse platea dictumst. Maecenas ac odio non lorem tincidunt ultricies quis."
	#source: https://www.lipsum.com/feed/html

	key = "hmsure"
	test = encrypt_vignere(valid_text, key)
	print (vignere_attack(test))

	#invalid test
	test = encrypt_vignere(invalid_text, key)
	print (vignere_attack(test) == None)

if __name__ == "__main__":
	main()

